# ARC Nodemailer

Entorno de pruebas para testear el comportamiento de las story cards en clientes de correo


Instalación

```sh
 node npm install 
```

### Preview con etherreal

  - Al ejecutar el comando creamos un envio de correo dinamico con posibilidad de previsualización (la consola develve la URL donde se puede ver el diseño)
  
  ```sh
   node email_preview
  ```
  
  
### Envio de email con cuenta Gmail
  
   - Enviamos un email con la cuenta de Gmail configurada el template adjunto que diseñamos
    
   ```sh
    node send_email
   ```





**Enlaces**

https://ethereal.email/message/XbsBLHdneCuionJVXbsBLddc9CwJsG6bAAAAAdLiK9MklLR8YTgE-TW-1rI?tab=header

https://ethereal.email/

https://nodemailer.com/about/



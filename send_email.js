'use strict';
const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('./configuration');

let htmlContent;

fs.readFile(path.template, "utf-8", (err, data) => {

    htmlContent = data;
});


// async..await is not allowed in global scope, must use a wrapper
async function main() {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport(
        {

            host: path.gmail.host, // Gmail Host
            port: path.gmail.port, // Port
            secure: path.gmail.secure, // this is true as port is 465
            auth: {
                user: path.gmail.auth.user, //Gmail username
                pass: path.gmail.auth.pass // Gmail password
            }

        }
    );


    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"El PAÍS" <proyectosjvl@gmail.com', // sender address
        to: 'jacobvl@gmail.com, jvlopez@clb.t-prisa.com', // list of receivers
        subject: 'NewsLetters EL PAÍS', // Subject line
        text: 'NewsLetters EL PAÍS', // plain text body
        html: htmlContent // html body
    });

    console.log('Message sent: %s', info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

main().catch(console.error);